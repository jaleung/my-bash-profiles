# My Bash Profiles

My Bash Profiles to be used across devices

# Usage
Clone the repo and source it in the machine's `.bash_profile` file under home directory

```
source ~/my-bash-profiles/.bashrc
```