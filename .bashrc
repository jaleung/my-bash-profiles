# Ionic and npm
# alias is='ionic serve'

# git related
alias gp='git pull'
alias gpp='git push'
alias gst='git status'
alias gb='git branch'
alias gmm='git merge master'

# Git shorthands
# commit only
gc() {
    git add .
    git commit -am "$1"
}

#commit and push
gcp() {
    git add .
    git commit -am "$1"
    git push
}

# ssh keygen
alias sshgen="ssh-keygen -t rsa -b 4096 -o -a 100"

# flush dns
alias flushdns="sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder; say DNS cache flushed"

# Quasar
alias qd='quasar dev'